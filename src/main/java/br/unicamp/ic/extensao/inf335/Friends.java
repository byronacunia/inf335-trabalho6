package br.unicamp.ic.extensao.inf335;

import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Friends {

	private static int season;

	public static void main(String[] args) {

		Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
		mongoLogger.setLevel(Level.SEVERE);

		System.out.println("*-------------------------------------------------*");
		System.out.println("*----------------     FRIENDS    -----------------*");
		System.out.println("*-------------------------------------------------*");
		System.out.println("* OPCÕES:\n" +
				           "* 1 à 10 (Quantidade de temporadas)");
		System.out.println("*-------------------------------------------------*");
		System.out.print(  "* Por favor, informar a temporada de Friends......: ");

		java.util.Scanner input = new java.util.Scanner(System.in);
		MongoClient mongoClient = MongoClients.create();
		MongoCollection<Document> dbCollection = getDbCollection(mongoClient);

		while (input.hasNext()) {
			season = input.nextInt();
			boolean seasonValidFlag = isInputValid(season);

			if (!seasonValidFlag) {
				System.out.println("Número da temporada inválido!");
				break;
			} else {
				MongoCursor<Document> seasonCursor = findEpisodesBySeason(dbCollection);

				System.out.println("*-------------------------------------------------*");
				System.out.println("* TEMPORADA " + season );
				System.out.println("*-------------------------------------------------*");
				int episodeNumber = 0;
				while (seasonCursor.hasNext()) {
					episodeNumber++;
					Document episode = seasonCursor.next();
					System.out.println("Episódio " + episodeNumber + ": " + episode.get("name"));
				}
			}
		}
		mongoClient.close();
	}

	private static MongoCursor<Document> findEpisodesBySeason(MongoCollection<Document> dbCollection) {
		BasicDBObject whereFilter = new BasicDBObject();
		whereFilter.put("season", season);

		return dbCollection.find(whereFilter).iterator();
	}

	private static MongoCollection<Document> getDbCollection(MongoClient mongoClient) {
		MongoDatabase db = mongoClient.getDatabase("friends");
		return db.getCollection("samples_friends");
	}

	private static boolean isInputValid(int season) {
		if (season > 10 || season < 1) {
			return false;
		}
		return true;
	}

}
